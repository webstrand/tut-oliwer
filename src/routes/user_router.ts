// imports
import express from "express";
import UserTokenModel from "../models/usertokens";
import UserAuthMiddleware from "../routes/middleware/user_auth";

// instances
const router = express.Router();

// fetch profile by auth
router.get("/profile", UserAuthMiddleware, async (request, response) => {
  // fetch from the database to ensure presence of token
  const fromDatabase = await UserTokenModel.findOne({ token: request.userToken });
  if (!fromDatabase) {
    return response.status(400).json({
      message: "token is inactive",
    });
  }

  // respond with the user's profile
  response.json({
    message: "successful profile fetch",
    profile: request.user,
  });
});

// export the router instance
export default router;
