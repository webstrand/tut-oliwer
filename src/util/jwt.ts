import { User } from "@models/user";

export declare function validateToken<T extends User>(token: T, secret: string): T;
